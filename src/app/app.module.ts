import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GreetingModule } from './modules/greeting/greeting.module';
import { GoodbyeModule } from './modules/goodbye/goodbye.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    GreetingModule,
    GoodbyeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
