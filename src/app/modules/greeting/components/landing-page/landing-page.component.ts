import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GreetingDialogComponent } from '../greeting-dialog/greeting-dialog.component';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  public openDialog() {
    const dialogRef = this.dialog.open(GreetingDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
