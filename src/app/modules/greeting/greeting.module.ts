import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { MaterialModule } from '../material/material.module';
import { GreetingDialogComponent } from './components/greeting-dialog/greeting-dialog.component';



@NgModule({
  declarations: [LandingPageComponent, GreetingDialogComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [LandingPageComponent],
  entryComponents: [GreetingDialogComponent]
})
export class GreetingModule { }
