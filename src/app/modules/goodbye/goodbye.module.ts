import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoodbyeLandingPageComponent } from './goodbye-landing-page/goodbye-landing-page.component';



@NgModule({
  declarations: [GoodbyeLandingPageComponent],
  imports: [
    CommonModule
  ],
  exports: [GoodbyeLandingPageComponent]
})
export class GoodbyeModule { }
