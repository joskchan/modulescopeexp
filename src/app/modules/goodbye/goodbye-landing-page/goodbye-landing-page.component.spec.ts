import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodbyeLandingPageComponent } from './goodbye-landing-page.component';

describe('GoodbyeLandingPageComponent', () => {
  let component: GoodbyeLandingPageComponent;
  let fixture: ComponentFixture<GoodbyeLandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodbyeLandingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodbyeLandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
