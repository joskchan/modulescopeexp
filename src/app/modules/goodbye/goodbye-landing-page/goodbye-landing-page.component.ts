import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GreetingDialogComponent } from '../../greeting/components/greeting-dialog/greeting-dialog.component';

@Component({
  selector: 'app-goodbye-landing-page',
  templateUrl: './goodbye-landing-page.component.html',
  styleUrls: ['./goodbye-landing-page.component.scss']
})
export class GoodbyeLandingPageComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  public openDialog() {
    const dialogRef = this.dialog.open(GreetingDialogComponent, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
